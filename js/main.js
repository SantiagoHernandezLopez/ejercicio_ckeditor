//El textarea en el que vamos a escribir.
if($("textarea").length > 0){
    CKEDITOR.replace("mensajesMain")
}

$(document).on("click",".btn", function(){

    var formData = new FormData($("#contact-form")[0]);

    //Recoge el contenido de  dentro del CKEditor.
    var textdata = CKEDITOR.instances.mensajesMain.getData();

    //Recoge el contenido de #title-main y lo guarda en la variable titledata.
    var titledata = $("#title-main").val();

    //Impide que los datos se pongan en la URL
    event.preventDefault();

    //Se guarda el contenido del CKEditor para poder imprimirlos en otro sitio.
    formData.append("mensajesMain", textdata)

    //Se imprimen todos los datos en las casilas correspondientes.
    $("#title1").val(titledata);
    $("#title2").val(titledata);
    $("#mensajes1").text(textdata);
    $("#mensajes2").text(textdata);

});